package net.sytes.rokkosan.mygetpermission

// 2022 Mar. 21.
// 2022 Mar. 11.
// Ryuichi Hashimoto.

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import net.sytes.rokkosan.mygetpermission.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val myPermission = Manifest.permission.READ_EXTERNAL_STORAGE
    private var isCheckedNotAskAgain: Boolean = false
    private val sharedPref: SharedPreferences by lazy { this.getSharedPreferences("myPref", Context.MODE_PRIVATE) }

    // registerForActivityResult()を定義してlaunch()を呼び出すと、
    // パーミッションが許可されていない時はユーザーにパーミッション許可を促すダイアログが表示される。
    // パーミッションが「許可された時」「許可されなかった時」のコールバック関数を記述する
    private val launcher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { granted ->
        if (granted) {
            // パーミッションが取得されている時
            // パーミッション取得後にすべき処理に移る
            isCheckedNotAskAgain = false
            with (sharedPref.edit()) {
                putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                apply()
            }
            myWorkWithPermission()
        } else {
            // パーミッションを取得していない
            if (shouldShowRequestPermissionRationale(myPermission)) {
                // パーミッション取得を拒否され、
                // 「今後表示しない」は選択されていない
                isCheckedNotAskAgain = false
                with (sharedPref.edit()) {
                    putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                    apply()
                }
            } else {
                // パーミッション取得を拒否され、
                // 「今後表示しない」が選択されて再リクエストも拒否されている
                isCheckedNotAskAgain = true
                with (sharedPref.edit()) {
                    putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                    apply()
                }
            }
            myWorkWithoutPermission()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        isCheckedNotAskAgain = sharedPref.getBoolean("isCheckedNotAskAgain", false)

        binding.btnGetPermission.setOnClickListener{
            getMyPermission()
        }
    }

    /*
     * パーミッションを取得するメソッド
     *   パーミッションが取得されれば
     *
     *   パーミッションが取得されなければ
     *
    */
    private fun getMyPermission() {
        if ( ContextCompat.checkSelfPermission(this, myPermission) ==
            PackageManager.PERMISSION_GRANTED) {
            // 既にパーミッションが取得されている時
            // パーミッション取得後にすべき処理に移る
            isCheckedNotAskAgain = false
            with (sharedPref.edit()) {
                putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                apply()
            }
            myWorkWithPermission()
        } else {
            // パーミッション未取得
            if (isCheckedNotAskAgain) {
                // 過去に権限取得を拒否され、「今後表示しない」が選択されて再リクエストも拒否されている
                // ダイアログでユーザーに説明し、ユーザーが望めば設定画面を表示する
                val openConfigDialogFragment = OpenConfigDialogFragment()
                openConfigDialogFragment.show(supportFragmentManager, "simple")

                // ダイアログ後の処理
                if ( ContextCompat.checkSelfPermission(this, myPermission) ==
                        PackageManager.PERMISSION_GRANTED) {
                    // （設定画面で）パーミッションを取得している
                    isCheckedNotAskAgain = false
                    with (sharedPref.edit()) {
                        putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                        apply()
                    }
                    myWorkWithPermission()
                } else {
                    // パーミッション未取得。
                    // shouldShowRequestPermissionRationale()に応じて
                    // isCheckedNotAskAgainを設定し保存する
                    if (shouldShowRequestPermissionRationale(myPermission)) {
                        // 「今後表示しない」は選択されていない
                        isCheckedNotAskAgain = false
                        with (sharedPref.edit()) {
                            putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                            apply()
                        }
                    } else {
                        // 「今後表示しない」が選択されている
                        isCheckedNotAskAgain = true
                        with (sharedPref.edit()) {
                            putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                            apply()
                        }
                    }
                    myWorkWithoutPermission()
                }
            } else {
                // パーミッションを取得しておらず、
                // 過去に権限取得で再リクエストの拒否はしていない。
                launcher.launch(myPermission)
            }
        }
    }

    // パーミッション取得後に行うこと
    private fun myWorkWithPermission(){
        binding.tvResult.text = getString(R.string.got_permission)
    }

    // パーミッション取得しなかった時に行うこと
    private fun myWorkWithoutPermission(){
        binding.tvResult.text = getText(R.string.failed_permission)
    }
}